package com.yonder.myproject;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.yonder.myproject.model.Post;
import com.yonder.myproject.repository.PostRepository;

@Path("posts")
public class Posts {
	
	@POST
	@Path("/newPost/{username}/{message}")
	public String newPost(@PathParam("username")String username,@PathParam("message")String message) {
		Post post = new Post(username,message);
		System.out.println("Username = " + post.getUsername() );
		System.out.println("Message = " + post.getMessage());
		return post.toString();
	}
	@Path("/getAll")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getAll(){
		PostRepository pR = new PostRepository();
		
		return pR.findAll().toString();
	}
	@Path("/getById/{id}")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getByID(@PathParam("id") int id) {
		PostRepository pR = new PostRepository();
		List<Post> post = pR.findAll();
		
		Post p = post.get(id);
		if (p!=null) {
			return p.toString();
		}else {
			return "Not found";
		}
		}
		
	}

