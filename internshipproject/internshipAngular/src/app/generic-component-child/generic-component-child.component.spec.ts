import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericComponentChildComponent } from './generic-component-child.component';

describe('GenericComponentChildComponent', () => {
  let component: GenericComponentChildComponent;
  let fixture: ComponentFixture<GenericComponentChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericComponentChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericComponentChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
