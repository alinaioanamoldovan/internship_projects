import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { GenericComponentChildComponent } from './generic-component-child/generic-component-child.component';
import { GenericComponentParentComponent } from './generic-component-parent/generic-component-parent.component';


@NgModule({
  declarations: [
    AppComponent,
    GenericComponentChildComponent,
    GenericComponentParentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
